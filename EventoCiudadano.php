<!DOCTYPE html>
<html>
    <head>
        <title>En Sus Manos - Ver Evento Por Ciudadano</title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/search.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/Pagina.css" type="text/css" media="all" />
        <link href="css/font-awesome.css" rel="stylesheet">
        <script src="js/Servicios.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
        <script type="text/javascript" src="js/sweetalert.min.js"></script>
    </head>

    <body>
        <div class="main-content" id="home">
            <header class="header">
                <div class="container">
                    <nav class="py-3">
                        <div id="logo">
                            <h1> <a class="navbar-brand" href="Inicio.php">En Sus Manos</a>
                            </h1>
                        </div>
                        <label for="drop" class="toggle">Menu</label>
                        <input type="checkbox" id="drop" />
                        <ul class="menu mt-2">
                            <li class="active"><a href="Inicio.php">Inicio</a></li>
                            <li><a href="#sobrenosotros">Sobre Nosotros</a></li>
                            <li><a href="#contacto">Contacto</a></li>
                            <li>
                                <label for="drop-2" class="toggle">Drop Down <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
                                <a href="#">Opciones <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                                <input type="checkbox" id="drop-2" />
                                <ul>
                                    <li><a href="#">Crear Evento</a>
                                    </li>
                                    <li><a href="BuscarCiudadano.php">Buscar Ciudadano</a>
                                    </li>
                                    <li><a href="index.php">Salir</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
        </div>
        <section class="about py-lg-5 py-md-5 py-5">
            <div class="container">
                <div class="inner-sec-w3pvt py-lg-5 py-3">
                    <div class="container h-100">
                        <div class="d-flex justify-content-center h-100">
                            <div class="searchbar">
                                <input class="search_input" type="text" name="identificacion" id="identificacion" placeholder="Número de Identificación" required="">
                                <a class="search_icon" id="btnBuscar" onclick="eventoCiudadano()"><i class="fas fa-search"></i></a>
                            </div>
                        </div>
                    </div><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Id Evento</th>
                                <th scope="col">Identificación</th>
                                <th scope="col">Fecha Evento</th>
                                <th scope="col">Hora Evento</th>
                                <th scope="col">Barrio</th>
                                <th scope="col">Situación</th>
                                <th scope="col">Puntaje</th>
                            </tr>
                        </thead>
                        <tbody id="tablaEvento">

                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center h-100">
                        <form action="EventoCiudadano.php">
                            <button type="submit" class="btn btn-outline-success">Nueva Búsqueda</button>
                        </form>                        
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
