<!DOCTYPE html>
<html>
    <head>
        <title>En Sus Manos - Página Principal</title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
        <link href="css/font-awesome.css" rel="stylesheet">
    </head>

    <body>
        <div class="main-content" id="home">
            <header class="header">
                <div class="container">
                    <nav class="py-3">
                        <div id="logo">
                            <h1> <a class="navbar-brand" href="Inicio.php">En Sus Manos</a>
                            </h1>
                        </div>
                        <label for="drop" class="toggle">Menu</label>
                        <input type="checkbox" id="drop" />
                        <ul class="menu mt-2">
                            <li class="active"><a href="Inicio.php">Inicio</a></li>
                            <li><a href="#sobrenosotros">Sobre Nosotros</a></li>
                            <li><a href="#contacto">Contacto</a></li>
                            <li>
                                <label for="drop-2" class="toggle">Drop Down <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
                                <a href="#">Opciones <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                                <input type="checkbox" id="drop-2" />
                                <ul>
                                    <li><a href="#">Crear Evento</a>
                                    </li>
                                    <li><a href="BuscarCiudadano.php">Buscar Ciudadano</a>
                                    </li>
                                    <li><a href="EventoCiudadano.php">Evento Ciudadano</a>
                                    </li>
                                    <li><a href="index.php">Salir</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>
            <section class="banner">
                <div class="container">
                    <div class="row banner-grids">
                        <div class="col-lg-6 banner-info-w3ls">
                            <h3>Incentiva la buena conducta de los ciudadanos</h3>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <section class="about py-lg-5 py-md-5 py-5" id="sobrenosotros">
            <div class="container">
                <div class="inner-sec-w3pvt">
                    <h3 class="tittle text-center">Sobre Nosotros </h3>
                    <div class="feature-grids row mt-3 mb-lg-5 mb-3 mt-lg-5 text-center">
                        <p>EnSusManos es una iniciativa del Ministerio de Transporte, el Ministerio de educación, el Ministerio de TIC y la Universidad Minuto de Dios, que
                            busca incentivar la buena conducta de los ciudadanos de San Jorge, a través de un sistema de puntos al ciudadano. </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="hand-crafted py-5" id="contacto">
            <div class="container py-lg-5">
                <div class="row accord-info">
                    <div class="col-lg-6 pl-md-5">
                        <h3 class="mb-md-5 tittle">Contacto</h3>
                        <p>Dirección : Calle falsa 123, Bogotá - Colombia</p>
                        <p>Teléfono : +57 9825281</p>
                        <p>Correo : <a href="#">info@ensusmanos.com</a></p>
                    </div>
                    <div class="col-lg-6 banner-image">
                        <div class="img-effect">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d254508.39280724886!2d-74.24789564207374!3d4.648625941680665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9bfd2da6cb29%3A0x239d635520a33914!2zQm9nb3TDoQ!5e0!3m2!1ses-419!2sco!4v1554956460627!5m2!1ses-419!2sco" width="600" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer>
            <div class="cpy-right py-3">
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-8">
                            <p class="copy-w3layouts">© 2019 En Sus Manos.</p>
                        </div>
                    </div>
                </div>
            </div>    
        </footer>
    </body>
</html>