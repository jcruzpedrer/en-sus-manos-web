$(document).ready(function () {
    $('#btnLogin').click(function () {
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: "http://localhost:8080/EnSusManos/v1/registros/login?nombreUsuario=" + $('#username').val() + "&contrasena=" + $('#password').val(),
            success: function (data) {
                if (data && data.length) {
                    setTimeout(function () {
                        swal({
                            position: "top-end",
                            type: "success",
                            title: "Ingresando al Portal......",
                            showConfirmButton: false,
                            timer: 1500
                        }, function () {
                            window.location = "Inicio.php";
                        });
                    }, 500);
                } else {
                    setTimeout(function () {
                        swal({
                            position: "top-end",
                            type: "error",
                            title: "Usuario y/o Contraseña Incorrectas",
                            showConfirmButton: false,
                            timer: 1500,
                            customClass: {
                                popup: 'animated tada'
                            }
                        }, function () {
                            window.location = "Login.php";
                        });
                    }, 500);
                }
            }
        });
        return false;
    });
});

function buscarCiudadano() {
    var id = Number($('#identificacion').val());
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "http://localhost:8080/EnSusManos/v1/ciudadanos/" + id,
        success: function (data) {
            $('.id').append(data.identificacion);
            $('.nombre').append(data.nombre);
            $('.apellido').append(data.apellido);
            $('.direccion').append(data.direccion);
            $('.ciudad').append(data.ciudad);
            $('.telefono').append(data.telefono);
        },
        error: function () {
            setTimeout(function () {
                swal({
                    position: "top-end",
                    type: "error",
                    title: "Usuario No Encontrado",
                    showConfirmButton: false,
                    timer: 1500,
                    customClass: {
                        popup: 'animated tada'
                    }
                }, function () {
                    window.location = "BuscarCiudadano.php";
                });
            }, 500);
        }
    });
}

function eventoCiudadano() {
    var id = Number($('#identificacion').val());
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "http://localhost:8080/EnSusManos/v1/eventos/ciudadanos/" + id,
        success: function (llenar) {
            $.each(llenar, function (key, value) {
                $("#tablaEvento").append("<tr><td>" + value['idEvento'] + "</td><td>" + value['idCiudadano'] + "</td><td>" + value['fecha'] + "</td><td>" + value['hora'] + "</td><td>" + value['barrio'] + "</td><td>" + value['situacion'] + "</td><td>" + value['puntaje'] + "</td></tr>");
            });
        },
        error: function () {
            swal({
                position: "top-end",
                type: "error",
                title: "Evento No Encontrado",
                showConfirmButton: false,
                timer: 1500,
                customClass: {
                    popup: 'animated tada'
                }
            }, function () {
                window.location = "EventoCiudadano.php";
            });
        }
    });
}