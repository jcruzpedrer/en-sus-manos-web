<!DOCTYPE html>
<html>
    <head>
        <title>En Sus Manos - Login</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link href="css/Login.css" rel='stylesheet' type='text/css'/>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/Servicios.js"></script>
        <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
        <script type="text/javascript" src="js/sweetalert.min.js"></script>
    </head>

    <body>
        <div class="main-content" id="home">
            <header class="header">
                <div class="container">
                    <nav class="py-3">
                        <div id="logo">
                            <h1> <a class="navbar-brand" href="index.php">En Sus Manos</a>
                            </h1>
                        </div>
                    </nav>
                </div>
            </header>

            <section class="banner">
                <div class="container">
                    <div class="row banner-grids">
                        <div class="col-lg-6 banner-info-w3ls">
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="login-form mx-auto d-block w-100">
                    <div class="page-header text-center">
                        <h1>Ingreso</h1><br>
                    </div>
                    <form method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="control-label">
                            </div>
                            <div class="controls">
                                <input name="username" id="username" class="validate-username required form-control invalid" size="25" required="required" aria-required="true" autofocus="" aria-invalid="true" type="text" placeholder="Nombre de Usuario">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <input name="password" id="password" class="validate-password required form-control" size="25" maxlength="99" required="required" aria-required="true" type="password" placeholder="Contraseña">
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#">Crear Cuenta</a>
                        </div>
                        <div class="controls">
                            <button type="submit" class="btn btn-primary" name="btnLogin" id="btnLogin">Ingresar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>